# WavesWalletSwapper

Moves funds/tokens from an address to another
Automatically avoids SPAM/Phishing Tokens


# Requirements

python3, pywaves

pip3 install pywaves

# Usage

Simply fill OLD_ADDRESS_PRIVATE_KEY and NEW_ADDRESS
Launch the script with python3 swapper.py

That's it!

# Donate

If you find this tool useful send me a tip to my waves address

3PQgaVmE7Zurn3xFMYtckah82PrWuaEcdhR

