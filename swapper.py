#!/bin/python3
# WalletSwapper for WAVES by deepred
# Join @MaDaMaLabs for info/updates
# Uses WAVES Spam Filters to avoid spam tokens

# USAGE:
# Fill OLD_ADDRESS_PRIVATE_KEY
# Fill NEW_ADDRESS

# Launch the script using python3

# Requirements:
# python3, pywaves
# pip3 install pywaves


import pywaves as pw
from time import sleep
import requests


OLD_ADDRESS_PRIVATE_KEY = ""
NEW_ADDRESS = ""


spam_list = requests.get("https://raw.githubusercontent.com/wavesplatform/waves-community/master/Scam%20tokens%20according%20to%20the%20opinion%20of%20Waves%20Community.csv").content.decode('utf8').strip().split('\n')


old_address = pw.Address(privateKey=OLD_ADDRESS_PRIVATE_KEY)

assets = {a['assetId']: a['balance'] for a in
                      requests.get("http://nodes.wavesnodes.com/assets/balance/%s" % old_address.address).json()['balances']}



for tokenid, amount in assets.items():
    if tokenid not in spam_list:
        print("[!] Sending %s to %s" % (tokenid, NEW_ADDRESS))
        try:
            old_address.sendAsset(pw.Address(NEW_ADDRESS), pw.Asset(tokenid), amount)
            sleep(3)
        except Exception as e:
            print("[x] Unable to send: %s" % str(e))

try:
    print("[!] Sending WAVES")
    old_address.sendWaves(recipient=pw.Address(NEW_ADDRESS), amount=(old_address.balance() - 100000))
except Exception as e:
    print("[x] Unable to send WAVES: %s" % str(e))

print("[*] Done, you welcome!")